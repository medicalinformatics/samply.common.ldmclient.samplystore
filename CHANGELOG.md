# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [3.0.0] - 2017-12-15
### Changed
- Adapt to v3(.0.1) of the interface ldmclient

## [2.0.0] - UNRELEASED
### Changed
- Adapt to v2 of the interface ldmclient

## [1.0.2] - 2017-10-10
### Changed
- Make error code constant public

## [1.0.1] - 2017-10-09
### Changed
- Use QueryConverter from share.dto

## [1.0.0] - 2017-10-06
### Added
- First implementation

