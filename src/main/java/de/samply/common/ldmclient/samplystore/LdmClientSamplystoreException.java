package de.samply.common.ldmclient.samplystore;

import de.samply.common.ldmclient.LdmClientException;

public class LdmClientSamplystoreException extends LdmClientException {
    public LdmClientSamplystoreException() {
        super();
    }

    public LdmClientSamplystoreException(String s) {
        super(s);
    }

    public LdmClientSamplystoreException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public LdmClientSamplystoreException(Throwable throwable) {
        super(throwable);
    }

    protected LdmClientSamplystoreException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

}
