package de.samply.common.ldmclient.samplystore;

import de.samply.common.ldmclient.LdmClient;
import de.samply.common.ldmclient.LdmClientUtil;
import de.samply.share.model.query.common.Error;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.queryresult.common.QueryResultStatistic;
import de.samply.share.model.query.common.View;
import org.apache.commons.io.IOUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeFalse;

/**
 * Test Class for the LDM Connector for Samply Store Backends
 */
public class LdmClientSamplystoreTest {

    private static final Logger logger = LoggerFactory.getLogger(LdmClientSamplystoreTest.class);
    private static final boolean CACHING = true;
    public static final int WAIT_FOR_SECS = 5;

    private static final String samplystoreBaseUrl = "http://192.168.57.101:8080/store-rest-1.4.0/";

    private static final String VIEW_FILENAME_OK = "exampleView.xml";
    private static final String VIEW_FILENAME_ERROR = "exampleViewError.xml";

    private LdmClient ldmClientSamplystore;
    private CloseableHttpClient httpClient;

    @Before
    public void setUp() throws Exception {
        this.httpClient = HttpClients.createDefault();
        this.ldmClientSamplystore = new LdmClientSamplystore(httpClient, samplystoreBaseUrl, CACHING);
    }

    @After
    public void tearDown() throws Exception {
        httpClient.close();
        ((LdmClientSamplystore)ldmClientSamplystore).cleanQueryResultsCache();
    }

    // Since postView calls postViewString, just run the test once
    public String testPostView() throws Exception {
        return testPostView(false);
    }

    public String testPostView(boolean error) throws Exception {
        String viewString;
        if (error) {
            viewString = readXmlFromFile(VIEW_FILENAME_ERROR);
        } else {
            viewString = readXmlFromFile(VIEW_FILENAME_OK);
        }
        View view = xmlToView(viewString);
        String location = ldmClientSamplystore.postView(view);
        assertFalse(LdmClientUtil.isNullOrEmpty(location));
        return location;
    }

    @Test
    public void testGetResult() throws Exception {
        String location = testPostView();
        assumeFalse(LdmClientUtil.isNullOrEmpty(location));
        // Just sleep for some seconds to give the ldm the time to process the query
        TimeUnit.SECONDS.sleep(WAIT_FOR_SECS);
        assertTrue(ldmClientSamplystore.getResult(location) instanceof QueryResult);
    }

    @Test
    public void testGetResultPage() throws Exception {
        String location = testPostView();
        assumeFalse(LdmClientUtil.isNullOrEmpty(location));
        // Just sleep for some seconds to give the ldm the time to process the query
        TimeUnit.SECONDS.sleep(WAIT_FOR_SECS);
        assertTrue(ldmClientSamplystore.getResultPage(location, 0) instanceof QueryResult);
    }

    @Test
    public void testGetStatsOrError() throws Exception {
        String location = testPostView();
        // Just sleep for some seconds to give the ldm the time to process the query
        TimeUnit.SECONDS.sleep(WAIT_FOR_SECS);
        Object statsOrError = ldmClientSamplystore.getStatsOrError(location);
        assertTrue((statsOrError instanceof QueryResultStatistic) || (statsOrError instanceof Error));
    }

    @Test
    public void testGetQueryResultStatistic() throws Exception {
        String location = testPostView();
        // Just sleep for some seconds to give the ldm the time to process the query
        TimeUnit.SECONDS.sleep(WAIT_FOR_SECS);
        assumeFalse(LdmClientUtil.isNullOrEmpty(location));
        assertTrue(ldmClientSamplystore.getQueryResultStatistic(location) instanceof QueryResultStatistic);
    }

    @Test
    public void testGetError() throws Exception {
        String location = testPostView(true);
        // Just sleep for some seconds to give the ldm the time to process the query
        TimeUnit.SECONDS.sleep(WAIT_FOR_SECS);
        assumeFalse(LdmClientUtil.isNullOrEmpty(location));
        assertTrue(ldmClientSamplystore.getError(location) instanceof Error);
    }

    private String readXmlFromFile(String filename) {
        String xml = "";

        try(InputStream is = getClass().getClassLoader().getResourceAsStream(filename)) {
            xml = IOUtils.toString(is, StandardCharsets.UTF_8);
        } catch(IOException ioEx) {
            ioEx.printStackTrace();
        }

        return xml;
    }

    private View xmlToView(String xml) throws JAXBException {
        InputSource inputSource = new InputSource(new StringReader(xml));

        JAXBContext jaxbContext = JAXBContext.newInstance(View.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (View) jaxbUnmarshaller.unmarshal(inputSource);
    }
}
